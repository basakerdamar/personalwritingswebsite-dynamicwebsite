<?php
if($_SERVER["HTTPS"] != "on")
{
    header("Location: https://" . $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"]);
    exit();
}
if (!isset($_SERVER['PHP_AUTH_USER'])) {
    header('WWW-Authenticate: Basic realm="Giriş"');
    header('HTTP/1.0 401 Unauthorized');
    exit;
} else {
  require 'OOconnect.php';

  $sql = "SELECT id, username FROM users WHERE username = ? AND password = ?";
  $stmt = $mysqli->prepare($sql);

  if(!$stmt) {
  	die("Prepare failed: (" . $mysqli->errno . ") " . $mysqli->error);
  }

  $username = $_SERVER['PHP_AUTH_USER'];
  $password = $_SERVER['PHP_AUTH_PW'];

  $bind_result = $stmt->bind_param("ss", $username, $password);
  if(!$bind_result) {
  	echo "Binding failed: (" . $stmt->errno . ") " . $stmt->error;
  }

  $execute_result = $stmt->execute();
  if(!$execute_result) {
    echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
  }

  $stmt->bind_result($id, $username);

  if($stmt->fetch()) {
    echo '<div class="alert alert-info" role="alert"> Merhaba '.$username.' </div>';
    $stmt->free_result();

    $stmt->close();

    $mysqli->close();
  }else {
    header('WWW-Authenticate: Basic realm="Giriş"');
    header('HTTP/1.0 401 Unauthorized');
    exit;
  }
}
require("connect.php");

?>
