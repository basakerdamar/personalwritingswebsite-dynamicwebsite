<?php require_once("auth.php"); ?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>İçerik Yönetimi</title>
	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

	<!-- jQuery library -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

	<!-- Latest compiled JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body>
<div class="container-fluid">
	<div class="page-header">
	  <h1>Erkan Erdamar <small>İçerik Yönetimi Sayfası</small></h1>
	</div>
	<div class="col-md-8">
	</div>
	<div class="col-md-4">
		<ul>
			<li><a href="makaleyonetim.php">Yazılar İçerik Yönetimi</a></li>
			<li><a href="siiryonetim.php">Şiir İçerik Yönetimi</a></li>
			<li><a href="ozgecmisyonetim.php">Özgeçmiş İçerik Yönetimi</a></li>
		</ul>
	</div>	
</div>


</body>
</html>
