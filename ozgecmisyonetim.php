<?php require_once("auth.php");
require("connect.php");
?>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

	<!-- jQuery library -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

	<!-- Latest compiled JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>
<div class="container-fluid">
	<div class="page-header">
  		<h1>Erkan Erdamar <small>Özgeçmiş İçerik Yönetimi Sayfası</small></h1>
	</div>
<div class="col-md-8">
	<form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" method="post">
		<textarea cols="100" rows="25" name="govde">
			<?php
				$query = mysqli_query( $link, "select metin from ozgecmis where id=1") 
					or die("<div class='alert alert-danger' role='alert'>Veritabanına ulaşılamadı.".mysqli_error(  )."</div>");
				$metin = mysqli_fetch_row(  $query);
				echo "$metin[0]";
			?>
		</textarea>
		<input type="submit" name="Kaydet" value="Kaydet">
	</form>
	<?php
		$kaydet = $_POST['Kaydet'];
		if ($kaydet) {
			$metin = trim (addslashes( $_POST['govde'] ) );
			mysqli_query( $link, "update ozgecmis set metin = '$metin' where id=1") or die("<div class='alert alert-danger' role='alert'>Değişiklikler kaydedilemedi. hata: ".mysqli_error(  )."</div>");
			echo "<div class='alert alert-success' role='alert'>Değişiklikler kaydedildi.</div>";
		}

	?>
</div>
<div class="col-md-4">
	<ul>
		<li><a href="makaleyonetim.php">Yazılar İçerik Yönetimi</a></li>
		<li><a href="siiryonetim.php">Şiir İçerik Yönetimi</a></li>
		<li><a href="ozgecmisyonetim.php">Özgeçmiş İçerik Yönetimi</a></li>
	</ul>
</div>	
</div>
</body>
</html>
