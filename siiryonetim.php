<?php require_once("auth.php");
require("connect.php");
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Şiir İçerik Yönetimi</title>
	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

	<!-- jQuery library -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

	<!-- Latest compiled JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>	
	<div class="container-fluid">
	<div class="page-header">
  		<h1>Erkan Erdamar <small>Şiirler İçerik Yönetimi Sayfası</small></h1>
	</div>
	<div class="row">
	<div class="col-md-8">
	<form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" method="post">
		<table>
		<tr><td>
		<select name="seciliicerikid">
			<?php
				$tumsatirlar = mysqli_query( $link, "select id, baslik from siirler order by baslik asc") or die ("1");
				// For SELECT, SHOW, DESCRIBE, EXPLAIN and other statements returning resultset, mysqli_query( $link, ) returns a resource on success, or FALSE on error.
      			while( $satir = mysqli_fetch_row(  $tumsatirlar) ){
         			echo "<option value=".$satir[0].">".$satir[1]."</option>";
          		}
			?>
			<option value="0">Yeni ekle...</option>
		</select>
		</td><td>
		<input type="submit" name="submit" value="Seç" >
		
		</td></tr>
		
		<?	
		extract($_POST);

		if($submit){
			if ($seciliicerikid != '0') {
				$secilicontent = mysqli_fetch_row(  mysqli_query( $link, "select * from siirler where id=$seciliicerikid" )) or die("3");
			}
		}
		?>
		</table></form>
		<table><form action="siiryonetim.php" method="post">
			<tr>
				<td>
					Başlık:
				</td>
				<td>
					<input type="text" name="yenibaslik" <?php 
					if ($seciliicerikid != '0') {
						echo "value=' $secilicontent[1]'";
					}
					else{
						echo "placeholder='Yeni başlık yaz'";
					}

					 ?>
					 >
				</td>
			</tr>
			<tr>
				<td>
					İçerik:
				</td>
				<td>
					<textarea cols="50" rows="22" name="yenicontent"><?php
					if ($seciliicerikid != '0') {
							echo $secilicontent[2]; 
					}
					 ?></textarea>
					 <input class="hiddenid" type="text" name="seciliicerikid" value="<?php echo $secilicontent[0]; ?>">
				</td>
			</tr>
			<tr><td>
				<input type="submit" name="degisikliklerikaydet" value="Değişiklikleri Kaydet">
				<input type="submit" name="Sil" value="Sil"></td>
			</tr>
			<?php
				$seciliicerikid = intval($seciliicerikid);
				if($degisikliklerikaydet){
					if ($yenibaslik=="" || $yenicontent=="") {
						echo "<div class='alert alert-danger' role='alert'>Başlık veya içerik alanı boş olamaz.</div>";
						exit();
					}
					// add escape characters
					$yenicontent = trim(addslashes($yenicontent)); 
					$$yenibaslik = trim(addslashes($yenibaslik));
					if($seciliicerikid == 0){
						mysqli_query( $link, "insert into siirler(baslik, content) values ('$yenibaslik', '$yenicontent')") or die ("<div class='alert alert-danger' role='alert'>Yeni şiir eklenemedi.".mysqli_error(  )."</div>");
						echo "<div class='alert alert-success' role='alert'>Yeni şiir kaydedildi.</div>";
					}
					else{
						
						mysqli_query( $link, "UPDATE siirler set content='$yenicontent', baslik='$yenibaslik' WHERE id=$seciliicerikid;") or die ("<div class='alert alert-danger' role='alert'>Değişiklikler kaydedilemedi. hata: ".mysqli_error(  )."</div>");
						echo "<div class='alert alert-success' role='alert'>Değişiklikler kaydedildi.</div>";
					}
				}
				if($Sil){
					$seciliicerikid = $_POST['seciliicerikid'];
					if ($seciliicerikid == 0) {
						echo "<div class='alert alert-danger' role='alert'>Sistemde kayıtlı olan bir makaleyi seçmelisin.</div>";
					}
					else{
						mysqli_query( $link, "delete from siirler where siirler.id=$seciliicerikid") //DELETE FROM `siirler` WHERE `siirler`.`id` = 22;
							or die("<div class='alert alert-danger' role='alert'>Silme işlemi başarısız oldu hata: ".mysqli_error(  )."</div>");
						echo "<div class='alert alert-success' role='alert'>Silme işlemi başarılı oldu.</div>";
					}
				} 
			?>
		</table>
	</form>
	</div>

<div class="col-md-4">
	<ul>
		<li><a href="makaleyonetim.php">Yazılar İçerik Yönetimi</a></li>
		<li><a href="siiryonetim.php">Şiir İçerik Yönetimi</a></li>
		<li><a href="ozgecmisyonetim.php">Özgeçmiş İçerik Yönetimi</a></li>
	</ul>
</div></div>
</body>
</html>
