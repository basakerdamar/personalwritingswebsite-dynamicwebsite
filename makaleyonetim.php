<?php require_once("auth.php");
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Yazı İçerik Yönetimi</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

	<!-- jQuery library -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

	<!-- Latest compiled JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>	
	<div class="container-fluid">
	<div class="page-header">
  		<h1>Erkan Erdamar <small>Yazılar İçerik Yönetimi Sayfası</small></h1>
	</div>
	<div class="col-md-8">
	<form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" method="post">
		<table>
		<tr><td>
		<select name="seciliicerikid">
			<?php
				$tumsatirlar = mysqli_query($link, "select id, baslik from yazilar order by baslik asc") or die ("Makale bulunamadı.");
				// For SELECT, SHOW, DESCRIBE, EXPLAIN and other statements returning resultset, mysqli_query( $link, ) returns a resource on success, or FALSE on error.
      			while( $satir = mysqli_fetch_row($tumsatirlar) ){
         			echo "<option value=".$satir[0].">".$satir[1]."</option>";
          		}
			?>
			<option value="0">Yeni ekle...</option>
		</select>
		</td><td>
		<input type="submit" name="submit" value="Seç" >
		
		</td></tr>
		
		<?	
		extract($_POST);
		if($submit){
				$seciliicerikid = intval($seciliicerikid);
				if ($seciliicerikid != '0') {
					$secilicontent = mysqli_fetch_row(mysqli_query($link, "select * from yazilar where id=$seciliicerikid" )) or die("<div class='alert alert-danger' role='alert'>Database ile ilgili sorun oluştu. Başak'a haber ver veya <a href='makaleyonetim.php'>geri git.</a></div>".mysqli_error());
			}
		}
		?>
		</table></form>
		<table><form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" method="post">
			<tr>
				<td>
					Başlık:
				</td>
				<td>
					<input type="text" name="yenibaslik" value="<?php echo $secilicontent[1]; ?>" size=80>
				</td>
			</tr>
			<tr>
				<td>
					İçerik:
				</td>
				<td>
					<textarea cols="100" rows="25" name="yenicontent"><?php	echo $secilicontent[2]; ?></textarea>
					<input class="hiddenid" type="text" name="seciliicerikid" value="<?php echo $secilicontent[0]; ?>">
				</td>
			</tr>
			<tr><td></td><td>
				<input type="submit" name="degisikliklerikaydet" value="Değişiklikleri Kaydet">
				<input type="submit" name="Sil" value="Sil">
				</td>
			</tr>
			<?php
				if($degisikliklerikaydet){
					$seciliicerikid = $_POST['seciliicerikid'];
					$yenibaslik = (addslashes( $_POST['yenibaslik']));
					$yenicontent = (addslashes( $_POST['yenicontent']));

					if ($yenibaslik=="" || $yenicontent=="") {
						echo "<div class='alert alert-danger' role='alert'>Başlık veya içerik alanı boş olamaz.</div>";
						exit();
					}

					//$yenicontent = addslashes($yenicontent);
					//$yenibaslik = addslashes($yenibaslik);
					
					if ($seciliicerikid != 0) {
						mysqli_query($link, "UPDATE yazilar set content='$yenicontent', baslik='$yenibaslik' WHERE id=$seciliicerikid;") 
							or die ("<br/>Değişiklikler kaydedilemedi.<br/>".mysqli_error(  ));
						echo "<div class='alert alert-success' role='alert'>Değişiklikler kaydedildi.</div>";
					}
					else{
						mysqli_query($link, "insert into yazilar values('', '$yenibaslik', '$yenicontent') ") 
							or die ("<div class='alert alert-danger' role='alert'>Yeni makale eklenemedi.".mysqli_error(  )."</div>");
						echo "<div class='alert alert-success' role='alert'>Değişiklikler kaydedildi.</div>";
					}
					
				}
				if($Sil){
					$seciliicerikid = $_POST['seciliicerikid'];
					if ($seciliicerikid == 0) {
						echo "<div class='alert alert-danger' role='alert'>Sistemde kayıtlı olan bir makaleyi seçmelisin.</div>";
					}
					else{
						mysqli_query($link, "delete from yazilar where yazilar.id=$seciliicerikid") //DELETE FROM `yazilar` WHERE `yazilar`.`id` = 22;
							or die("<div class='alert alert-danger' role='alert'>Silme işlemi başarısız oldu hata: ".mysqli_error()."</div>");
						echo "<div class='alert alert-success' role='alert'>Silme işlemi başarılı oldu.</div>";
					}
				}
			?>
	</table></form></div>
	<div class="col-md-4">
		<ul>
			<li><a href="makaleyonetim.php">Yazılar İçerik Yönetimi</a></li>
			<li><a href="siiryonetim.php">Şiir İçerik Yönetimi</a></li>
			<li><a href="ozgecmisyonetim.php">Özgeçmiş İçerik Yönetimi</a></li>
		</ul>
	</div>	
	</div>

</body>
</html>
